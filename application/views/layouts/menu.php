<?php /** @var string $active */ ?>

<nav class="navbar navbar-dark navbar-expand-lg mb-3 pt-3 bg-primary" style="border-bottom: 1px solid #ddd;">
	<div class="container-fluid">
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse px-lg-5 px-md-4 px-sm-1" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item">
					<a class="nav-link <?= $active == 'dashboard' ? 'active' : '' ?>" href="<?= site_url('dashboard/index') ?>">Dashboard</a>
				</li>
				<?php /*
				<li class="nav-item">
					<a class="nav-link <?= $active == 'barang' ? 'active' : '' ?>" href="<?= site_url('barang/index') ?>">ModelBarang</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?= $active == 'supplier' ? 'active' : '' ?>" href="<?= site_url('supplier/index') ?>">Supplier</a>
				</li>
				*/ ?>
				<li class="nav-item">
					<a class="nav-link <?= $active == 'pembelian' ? 'active' : '' ?>" href="<?= site_url('pembelian/index') ?>">Pembelian</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?= $active == 'penerimaan' ? 'active' : '' ?>" href="<?= site_url('penerimaan/index') ?>">Penerimaan</a>
				</li>
				<?php /*
				<li class="nav-item">
					<a class="nav-link <?= $active == 'laporan' ? 'active' : '' ?>" href="<?= site_url('laporan/index') ?>">Laporan</a>
				</li>
				*/ ?>
				<li class="nav-item dropdown">
					<?php $listReport = ['report-pembelian', 'report-penerimaan'] ?>
					<a class="nav-link dropdown-toggle <?= in_array($active, $listReport, true) ? 'active' : '' ?>" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						Laporan
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item <?= $active == 'report-pembelian' ? 'active' : '' ?>" href="<?= site_url('laporan/index/pembelian') ?>">Pembelian</a>
						</li>
						<li>
							<a class="dropdown-item <?= $active == 'report-penerimaan' ? 'active' : '' ?>" href="<?= site_url('laporan/index/penerimaan') ?>">Penerimaan</a>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<?php $listMaster = ['barang', 'supplier'] ?>
					<a class="nav-link dropdown-toggle <?= in_array($active, $listMaster, true) ? 'active' : '' ?>" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						Master
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item <?= $active == 'barang' ? 'active' : '' ?>" href="<?= site_url('barang/index') ?>">Barang</a>
						</li>
						<li>
							<a class="dropdown-item <?= $active == 'supplier' ? 'active' : '' ?>" href="<?= site_url('supplier/index') ?>">Supplier</a>
						</li>
					</ul>
				</li>
			</ul>
			<a href="<?= site_url('auth/logout') ?>" class="btn btn-sm btn-light text-secondary float-right">Logout</a>
		</div>
	</div>
</nav>

<!--alert-->
<div class="row" style="margin-top: -15px !important;">
	<?php if (@$this->session->flashdata()) {
		foreach ($this->session->flashdata() as $key => $value) { ?>
			<div class="container">
				<div class="col-12 alert alert-<?=$key?> px-lg-5 px-md-4 px-sm-1 text-center" role="alert">
					<?= $this->session->flashdata($key) ?>
				</div>
			</div>
		<?php }
	} ?>
</div>

<!--need jquery-->
<script type="text/javascript">
	$(document).ready(function () {
		setTimeout(() => {
			$('.alert').fadeOut();
		}, 1500)
	});
</script>

<!-- wrapper for content -->
<!-- end wrapper is in the footer -->
<div id="wrapper" class="mt-5 px-lg-5 px-md-4 px-sm-1 mb-5">

