<div class="row">
	<div class="container">
		<div class="card">
			<div class="card-header bg-light mb-2">
				<h5 class="card-title mt-2">
					Laporan Penerimaan
				</h5>
			</div>
			<div class="card-body">
				<div class="col-12 mb-5">
					<form action="<?= site_url('laporan/generate-penerimaan') ?>" method="post">
						<input type="hidden" name="tanggal_mulai" value="<?= @$post['tanggal_mulai'] ?>">
						<input type="hidden" name="tanggal_akhir" value="<?= @$post['tanggal_akhir'] ?>">
						<input type="hidden" name="id_barang" value="<?= @$post['id_barang'] ?>">
						<input type="hidden" name="export_excel" value="true">
						<button type="submit" class="btn btn-success">
							<i class="bi-file-earmark-excel-fill me-2"></i>Export
						</button>
					</form>
				</div>
				<div class="col-12">
					<?php $no = 1; ?>
					<table class="table table-bordered table-hover pt-4">
						<thead>
						<tr>
							<th scope="col" rowspan="2">#</th>
							<th scope="col" colspan="2" style="text-align: center">Surat Jalan</th>
							<th scope="col" rowspan="2">No. Dokumen</th>
							<th scope="col" colspan="2" style="text-align: center">Barang</th>
							<th scope="col" colspan="2" style="text-align: center">Quantity</th>
							<th scope="col" rowspan="2">Sisa</th>
						</tr>
						<tr>
							<th scope="col">Tanggal</th>
							<th scope="col">Nomor</th>
							<th scope="col">Kode</th>
							<th scope="col">Nama</th>
							<th scope="col">Order</th>
							<th scope="col">Terima</th>
						</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($allData as $data) { ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $data['tanggal_sj'] ?></td>
									<td><?= $data['no_sj'] ?></td>
									<td><?= $data['no_dokumen'] ?></td>
									<td><?= $data['kode_barang'] ?></td>
									<td><?= $data['nama_barang'] ?></td>
									<td><?= $data['quantity_order']?></td>
									<td><?= $data['quantity_accept']?></td>
									<td><?= $data['sisa']?></td>
								</tr>
							<?php } ?>
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('table').dataTable();
	})
</script>
