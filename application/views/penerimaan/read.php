<div class="row">
	<div class="col-12">
		<form method="POST" action="<?= site_url('penerimaan/create-show-detail') ?>">
			<div class="card mb-4">
				<div class="card-header">
					<h5 class="card-title">
						Form Penerimaan Barang
					</h5>
				</div>
				<div class="card-body">
					<div class="mb-3">
						<label class="form-label"><b>No. Dokumen</b></label>
						<input type="text" class="form-control" name="no_dokumen" value="<?= @$data['no_dokumen_penerimaan'] ?>" disabled>
					</div>
					<div class="mb-3">
						<label class="form-label">Tanggal Terima</label>
						<input type="date" class="form-control" name="tanggal" value="<?= @$data['tanggal'] ?>" disabled>
					</div>
				</div>
				<hr>
				<div class="card-body">
					<div class="mb-3">
						<label class="form-label">Tanggal Surat Jalan</label>
						<input type="date" class="form-control disabled" name="tanggal_sj" value="<?= $data['tanggal_sj'] ?>" disabled>
					</div>
					<div class="mb-3">
						<label class="form-label">Nomor Surat Jalan</label>
						<input type="text" class="form-control disabled" name="no_sj" value="<?= $data['no_sj'] ?>" disabled>
					</div>
					<div class="mb-5">
						<label class="form-label"><b>Dokumen Pembelian</b></label>
						<select class="form-select disabled" name="" disabled>
							<option value="">- Pilih -</option>
							<?php foreach ($arrayDokumen as $dokumen) { ?>
								<?php $selected = ($dokumen['id'] == $data['id_pembelian']) ? 'selected' : ''; ?>
								<option value="<?= $dokumen['id'] ?>" <?= $selected ?>><?= $dokumen['no_dokumen'] ?></option>
							<?php } ?>
						</select>
					</div>
						<a href="<?= site_url('penerimaan/index') ?>" class="btn btn-dark">Kembali</a>
						<a href="<?= site_url('penerimaan/update/' . $data['id']) ?>" class="btn btn-warning">Ubah</a>
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Daftar Item</h5>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-light">
						<thead>
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">Barang</th>
							<th rowspan="2">Harga satuan</th>
							<th colspan="2" style="text-align: center">Quantity</th>
						</tr>
						<tr>
							<th>Total / Sisa</th>
							<th>Terima</th>
						</tr>
						</thead>
						<?php $index = 1; ?>
						<tbody>

						<?php foreach ($allPenerimaanDetail as $penerimaanDetail) { ?>
							<tr class="item">
								<td><span class="index"><?= $index ?></span></td>
								<td>
									<select class="form-select" name="id_barang" disabled>
										<option>- Pilih -</option>
										<?php foreach ($allBarang as $barang) { ?>
											<?php $selected = $barang['id'] == $penerimaanDetail['id_barang'] ? 'selected' : ''; ?>
											<option value="<?= $barang['id'] ?>" <?= $selected ?>><?= $barang['nama'] ?></option>
										<?php } ?>
									</select>
								</td>
								<td>
									<input type="text" class="form-control currency-input" name="harga" value="<?= 'Rp. ' . number_format($penerimaanDetail['harga_satuan'], 2, ".", ",") ?>" disabled>
								</td>
								<td>
									<?php $currentMaxQty = $penerimaanDetail['penerimaan_sisa'] + $penerimaanDetail['quantity_accept'] ?>
									<input type="text" class="form-control quantity-order" name="qty_pembelian" value="<?= $currentMaxQty ?>" disabled>
								</td>
								<td>
									<input type="number" class="form-control quantity-input" name="qty_terima" value="<?= $penerimaanDetail['quantity_accept'] ?>" disabled>
								</td>
								<input type="hidden" class="form-control quantity-remain disabled" name="sisa" value="" disabled>
							</tr>
							<?php $index++; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	input.disabled:hover {
		cursor: not-allowed !important;
	}
</style>

<script type="text/javascript">

	const fillElementSisa = (e) => {
		const parent = $(e).closest('tr');
		const nodeOrder = parent.find('.quantity-order');
		const maxOrder = $(nodeOrder).val();

		const nodeSisa = parent.find('.quantity-remain')

		const currentValue = $(e).val();
		if (currentValue <= 0) {
			console.log("min ", currentValue);
		}

		if (currentValue >= maxOrder) {
			console.log("max ", currentValue);
		}
		$(nodeSisa).val(maxOrder - currentValue);
	}

	$(document).ready(function () {
		$(document).ready(function() {
			$('select').select2({'disabled' : true});
		});
		$('input.quantity-input').each(function() {
			$(this).on({
				keyup: function() {
					fillElementSisa($(this))
				},
				blur: function() {

				}
			})
		})
	})
</script>
