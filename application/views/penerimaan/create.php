<div class="row">
    <div class="col-12">
        <form method="POST" action="<?= site_url('penerimaan/create') ?>">
            <div class="card mb-4">
                <div class="card-header">
                    <h5 class="card-title">
                        Form Penerimaan Barang
                    </h5>
                </div>
				<div class="card-body">
					<div class="mb-3">
						<label class="form-label">Tanggal Terima</label>
						<input type="date" class="form-control" name="tanggal" value="<?= @$post['tanggal'] ?>" required>
					</div>
				</div>
				<hr>
                <div class="card-body">                        
                    <div class="mb-3">
                        <label class="form-label">Tanggal Surat Jalan</label>
                        <input type="date" class="form-control" name="tanggal_sj" value="<?= @$post['tanggal_sj'] ?>" required>
                    </div>
					<div class="mb-3">
						<label class="form-label">Nomor Surat Jalan</label>
						<input type="text" class="form-control" name="no_sj" value="<?= @$post['no_sj'] ?>" required>
					</div>
					<div class="mb-5">

						<label class="form-label"><b>Dokumen Pembelian</b></label>
						<select class="form-select" name="id_pembelian" required>
							<option value="">- Pilih -</option>
							<?php foreach ($arrayDokumen as $dokumen) { ?>
								<?php $selected = (@$post['id_pembelian'] == $dokumen['id']) ? 'selected' : ''; ?>
								<option value="<?= $dokumen['id'] ?>" <?= $selected ?>><?= $dokumen['no_dokumen'] ?></option>
							<?php } ?>
						</select>
					</div>
                    <button type="submit" class="btn btn-success">Tamplikan</button>
                    <a href="<?= site_url('penerimaan/index') ?>" class="btn btn-dark">Kembali</a>
                </div>
            </div>

        </form>
    </div>
</div>

<script type="text/javascript" src="<?= base_url('public/libs/js/terbilang.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/libs/js/currencyFormatter.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('select').select2();
    });
</script>
