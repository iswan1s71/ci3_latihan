<div class="row">
	<div class="container">
		<div class="card">
			<div class="card-header bg-light mb-2">
				<h5 class="card-title mt-2">
					Daftar Penerimaan Barang
				</h5>
			</div>
			<div class="card-body">
				<div class="col-12 mb-5">
					<a href="<?= site_url('penerimaan/create') ?>" class="btn btn-primary">
						<i class="bi-plus me-2"></i>Tambah
					</a>
				</div>
				<div class="col-12">
					<?php $no = 1; ?>
					<table class="table table-bordered table-hover pt-4">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Tanggal</th>
								<th scope="col">No. Surat Jalan</th>
								<th scope="col">No. Dokumen</th>
								<th scope="col">Dokumen Pembelian</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach ($allPenerimaan as $penerimaan) { ?>
									<tr>
										<td><?= $no++ ?></td>
										<td><?= $penerimaan['tanggal'] ?></td>
										<td><?= $penerimaan['no_sj'] ?></td>
										<td><?= $penerimaan['no_dokumen_penerimaan'] ?></td>
										<td><?= $penerimaan['no_dokumen_pembelian'] ?></td>
										<td>
											<div class="container">
												<a href="<?= site_url('penerimaan/read/'.$penerimaan['id']) ?>" class="btn btn-sm btn-success"><i class="bi-eye"></i></a>
												<a href="<?= site_url('penerimaan/update/'.$penerimaan['id']) ?>" class="btn btn-sm btn-warning"><i class="bi-pencil"></i></a>
												<a href="<?= site_url('penerimaan/delete/'.$penerimaan['id']) ?>" class="btn btn-sm btn-danger"><i class="bi-trash"></i></a>
											</div>
										</td>
									</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.btn-disabled, .toggle:disabled:hover {
		cursor: not-allowed !important;
		background-color: #6c757d !important;
	}
</style>
<script type="text/javascript" src="<?= base_url('public/libs/js/instanceSubmitForm.js') ?>"></script>
<script>
	$(document).ready(function() {
		$('table').dataTable();
	})

</script>
