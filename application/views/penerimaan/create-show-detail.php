<div class="row">
    <div class="col-12">
        <form method="POST" action="<?= site_url('penerimaan/create-show-detail') ?>">
            <div class="card mb-4">
                <div class="card-header">
                    <h5 class="card-title">
                        Form Penerimaan Barang
                    </h5>
                </div>
				<div class="card-body">
					<div class="mb-3">
						<label class="form-label">Tanggal Terima</label>
						<input type="date" class="form-control disabled" name="tanggal" value="<?= @$post['tanggal'] ?>" readonly="">
					</div>
				</div>
				<hr>
                <div class="card-body">                        
                    <div class="mb-3">
                        <label class="form-label">Tanggal Surat Jalan</label>
                        <input type="date" class="form-control disabled" name="tanggal_sj" value="<?= @$post['tanggal_sj'] ?>" readonly="">
                    </div>
					<div class="mb-3">
						<label class="form-label">Nomor Surat Jalan</label>
						<input type="text" class="form-control disabled" name="no_sj" value="<?= @$post['no_sj'] ?>" readonly="">
					</div>
					<div class="mb-5">
						<label class="form-label"><b>Dokumen Pembelian</b></label>
						<select class="form-select disabled" name="" readonly="">
							<option value="">- Pilih -</option>
							<?php foreach ($arrayDokumen as $dokumen) { ?>
								<?php $selected = (@$post['id_pembelian'] == $dokumen['id']) ? 'selected' : ''; ?>
								<option value="<?= $dokumen['id'] ?>" <?= $selected ?>><?= $dokumen['no_dokumen'] ?></option>
							<?php } ?>
							<input type="hidden" name="id_pembelian" value="<?= $post['id_pembelian'] ?>">
						</select>
					</div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= site_url('penerimaan/index') ?>" class="btn btn-dark">Kembali</a>
                </div>
            </div>

			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Daftar Item</h5>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-light">
						<thead>
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">Barang</th>
							<th rowspan="2">Harga satuan</th>
							<th colspan="2" style="text-align: center">Quantity</th>
<!--							<th rowspan="2">Sisa</th>-->
						</tr>
						<tr>
<!--							<th>Order</th>-->
							<th style="text-align: left">Total / Sisa</th>
							<th style="text-align: left">Terima</th>
						</tr>
						</thead>
						<?php $index = 1; ?>
						<tbody>
						<?php // echo '<pre>'; var_dump($allPembelianDetail); echo '</pre>'; ?>
						<?php foreach ($allPembelianDetail as $pembelianDetail) { ?>
							<tr class="item">
								<td><span class="index"><?= $index ?></span></td>
								<td>
									<select class="form-select" name="id_barang" disabled>
										<option>- Pilih -</option>
										<?php foreach ($allBarang as $barang) { ?>
											<?php $selected = $barang['id'] == $pembelianDetail['id_barang'] ? 'selected' : ''; ?>
											<option value="<?= $barang['id'] ?>" <?= $selected ?>><?= $barang['nama'] ?></option>
										<?php } ?>
									</select>
								</td>
								<td>
									<input type="text" class="form-control currency-input" name="harga" value="<?= 'Rp. ' . number_format($pembelianDetail['harga'], 2, ".", ",") ?>" disabled>
								</td>
								<?php
									$maxQuantityInput = $pembelianDetail['qty'];
									$remain = $pembelianDetail['sisa_pembelian'];
									$done = false;
									if ($remain != null) {
										if ($remain == 0) {
											$done = true;
											$maxQuantityInput = 0;
										} else {
											$maxQuantityInput = $remain;
										}
									}

									$quantityInputDisabled = '';
									$readOnly = '';

									if ($done) {
										$quantityInputDisabled = 'disabled';
										$readOnly = 'readonly';
									}
								?>
								<td>
									<input type="text" class="form-control quantity-order" name="qty_pembelian" value="<?= $maxQuantityInput ?>" disabled>
								</td>
								<td>
									<input type="number" class="form-control quantity-input <?= $quantityInputDisabled ?>"
										name="penerimaan_detail[<?=$index?>][qty]" value=""
										   min="0"
										   max="<?= $maxQuantityInput ?>"
										   message="Melebihi batas"
										   <?= $readOnly ?>
										   required>
								</td>
								<input type="hidden" class="form-control quantity-remain" name="penerimaan_detail[<?=$index?>][sisa]">
								<input type="hidden" name="penerimaan_detail[<?=$index?>][id_pembelian_detail]" value="<?= $pembelianDetail['id'] ?>">
							</tr>
							<?php $index++; } ?>
						</tbody>
					</table>
				</div>
			</div>
        </form>
    </div>
</div>
<style>
	input.disabled:hover {
		cursor: not-allowed !important;
	}
</style>

<script type="text/javascript">

	const fillElementSisa = (e) => {
		const parent = $(e).closest('tr');
		const nodeOrder = parent.find('.quantity-order');
		const maxOrder = $(nodeOrder).val();

		const nodeSisa = parent.find('.quantity-remain')

		const currentValue = $(e).val();
		if (currentValue <= 0) {
			console.log("min ", currentValue);
		}

		if (currentValue >= maxOrder) {
			console.log("max ", currentValue);
		}
		$(nodeSisa).val(maxOrder - currentValue);
	}

	$(document).ready(function () {
		$(document).ready(function() {
			$('select').select2({'disabled' : true});
		});
		$('input.quantity-input').each(function() {
			$(this).on({
				keyup: function() {
					fillElementSisa($(this))
				},
				blur: function() {

				}
			})
		})
	})
</script>
