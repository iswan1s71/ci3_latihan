<div class="row">
	<div class="col-12">
		<form method="POST" action="<?= site_url('penerimaan/update') ?>">
			<div class="card mb-4">
				<div class="card-header">
					<h5 class="card-title">
						Form Penerimaan Barang
					</h5>
				</div>
				<div class="card-body">
					<div class="mb-3">
						<input type="hidden" name="id" value="<?= @$data['id'] ?>">
						<label class="form-label"><b>No. Dokumen</b></label>
						<input type="text" class="form-control" name="no_dokumen" value="<?= @$data['no_dokumen_penerimaan'] ?>" disabled>
					</div>
					<div class="mb-3">
						<label class="form-label">Tanggal Terima</label>
						<input type="date" class="form-control" name="tanggal" value="<?= @$data['tanggal'] ?>" required>
					</div>
				</div>
				<hr>
				<div class="card-body">
					<div class="mb-3">
						<label class="form-label">Tanggal Surat Jalan</label>
						<input type="date" class="form-control" name="tanggal_sj" value="<?= $data['tanggal_sj'] ?>" >
					</div>
					<div class="mb-3">
						<label class="form-label">Nomor Surat Jalan</label>
						<input type="text" class="form-control" name="no_sj" value="<?= $data['no_sj'] ?>" >
					</div>
					<div class="mb-5">
						<label class="form-label">No dokumen</label>
						<select class="form-select" name="" >
							<option value="">- Pilih -</option>
							<?php $selected = '' ?>
							<?php foreach ($arrayDokumen as $dokumen) { ?>
								<?php if ($data['id_pembelian'] == $dokumen['id']) $selected = 'selected'; ?>
								<option value="<?= $dokumen['id'] ?>" <?= $selected ?>><?= $dokumen['no_dokumen'] ?></option>
							<?php } ?>
						</select>
					</div>
					<input type="hidden" name="id" value="<?= $data['id'] ?>" />
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="<?= site_url('penerimaan/index') ?>" class="btn btn-dark">Kembali</a>
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Daftar Item</h3>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-light">
						<thead>
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">Barang</th>
							<th rowspan="2">Harga satuan</th>
							<th colspan="2" style="text-align: center">Quantity</th>
						</tr>
						<tr>
							<th>Total / Sisa</th>
							<th>Terima</th>
						</tr>
						</thead>
						<?php $index = 1; ?>
						<tbody>

						<?php foreach ($allPenerimaanDetail as $penerimaanDetail) { ?>
							<tr class="item">
								<td><span class="index"><?= $index ?></span></td>
								<td>
									<select class="form-select" name="id_barang" disabled>
										<option>- Pilih -</option>
										<?php foreach ($allBarang as $barang) { ?>
											<?php $selected = $barang['id'] == $penerimaanDetail['id_barang'] ? 'selected' : ''; ?>
											<option value="<?= $barang['id'] ?>" <?= $selected ?>><?= $barang['nama'] ?></option>
										<?php } ?>
									</select>
								</td>
								<td>
									<input type="text" class="form-control currency-input" name="harga" value="<?= 'Rp. ' . number_format($penerimaanDetail['harga_satuan'], 2, ".", ",") ?>" disabled>
								</td>
								<td>
									<input type="text" class="form-control quantity-order" name="qty_pembelian" value="<?= $penerimaanDetail['quantity_order'] ?>" disabled>
								</td>
								<td>
									<input type="number" class="form-control quantity-input" name="penerimaan_detail[<?=$index?>][qty]" value="<?= $penerimaanDetail['quantity_accept'] ?>">
								</td>
								<input type="hidden" class="quantity-remain" name="penerimaan_detail[<?=$index?>][sisa]" value="<?= $penerimaanDetail['penerimaan_sisa'] ?>" readonly>
								<input type="hidden" class="" name="penerimaan_detail[<?=$index?>][id_pembelian_detail]" value="<?= $penerimaanDetail['id_pembelian_detail'] ?>" readonly>
								<input type="hidden" name="penerimaan_detail[<?=$index?>][id]" value="<?= $penerimaanDetail['id'] ?>">
							</tr>
							<?php $index++; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	input.disabled:hover {
		cursor: not-allowed !important;
	}
</style>

<script type="text/javascript">

	const fillElementSisa = (e) => {
		const parent = $(e).closest('tr');
		const nodeOrder = parent.find('.quantity-order');
		const maxOrder = $(nodeOrder).val();

		const nodeSisa = parent.find('.quantity-remain')

		const currentValue = $(e).val();
		if (currentValue <= 0) {
			console.log("min ", currentValue);
		}

		if (currentValue >= maxOrder) {
			console.log("max ", currentValue);
		}
		$(nodeSisa).val(maxOrder - currentValue);
	}

	$(document).ready(function () {
		$(document).ready(function() {
			$('select').select2({'disabled' : true});
		});
		$('input.quantity-input').each(function() {
			$(this).on({
				keyup: function() {
					fillElementSisa($(this))
				},
				blur: function() {

				}
			})
		})
	})
</script>
