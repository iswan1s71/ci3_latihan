<div class="row mb-4">
	<div class="col-4">
		<div class="card mb-4 rounded-3 shadow-sm border-primary">
			<div class="card-header py-3 text-bg-primary border-primary">
				<h5 class="my-0 fw-normal">Barang</h5>
			</div>
			<div class="card-body">
				<span class="text-muted fw-light fs-5">Total</span>
				<h1 class="card-title pricing-card-title text-end"><?= count($allBarang) ?></h1>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card mb-4 rounded-3 shadow-sm border-warning">
			<div class="card-header py-3 text-bg-warning border-warning">
				<h5 class="my-0 fw-normal">Supplier</h5>
			</div>
			<div class="card-body">
				<span class="text-muted fw-light fs-5">Total</span>
				<h1 class="card-title pricing-card-title text-end"><?= count($allSupplier) ?></h1>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card mb-4 rounded-3 shadow-sm border-success">
			<div class="card-header py-3 text-bg-success border-success">
				<h5 class="my-0 fw-normal">Pembelian</h5>
			</div>
			<div class="card-body">
				<span class="text-muted fw-light fs-5">Total</span>
				<h1 class="card-title pricing-card-title text-end"><?= count($allPembelian) ?></h1>
			</div>
		</div>
	</div>
</div>

<div class="row mb-4">
	<div class="col-7">
		<canvas id="sale-per-bulan"></canvas>
	</div>
	<div class="col-5">
		<canvas id="sale-per-supplier"></canvas>
	</div>
</div>

<div class="row mb-4">
	<div class="col-12">
		<canvas id="sale-per-barang"></canvas>
	</div>
</div>

<script src="<?= base_url('public/libs/chartjs/dist/chart.umd.js') ?>"></script>
<script>
	const createChartPerBulan = () => {
		let labels = [];
		let _data = [];

		const dataBulan = <?= json_encode($grafik['totalPenjualanPerBulan']) ?>;
		dataBulan.forEach((e) => {
			labels.push(e?.periode);
			_data.push(e?.sum);
		})

		const data = {
			labels: labels,
			datasets: [{
				label: 'Penjualan per Bulan',
				data: _data,
				fill: false,
				borderColor: 'rgb(75, 192, 192)',
				tension: 0.1
			}]
		};
		new Chart(
			document.getElementById('sale-per-bulan'),
			{
				type: 'line',
				data: data
			}
		);
	}

	const createChartPerSupplier = () => {
		let colorTheme = [
			'rgb(255, 99, 132)',
			'rgb(54, 162, 235)',
			'rgb(255, 205, 86)',
			'forestgreen',
			'indigo',
			'khaki',
			'lavenderblush'
		];
		let labels = [];
		let _data = [];
		let _bgColors = [];

		const dataBulan = <?= json_encode($grafik['totalPenjualanPerSupplier']) ?>;
		dataBulan.forEach((e, index) => {
			labels.push(e?.nama);
			_data.push(e?.sum);
			_bgColors.push(colorTheme[index])
		})

		const data = {
			labels: labels,
			datasets: [{
				label: 'Penjualan per Supplier',
				data: _data,
				fill: false,
				backgroundColor: _bgColors,
			}]
		};
		new Chart(
			document.getElementById('sale-per-supplier'),
			{
				type: 'pie',
				data: data
			}
		);
		return;
		//const dataSupplier = <?php //= json_encode($grafik['totalPenjualanPerSupplier']) ?>//;
		//new Chart(
		//	document.getElementById('sale-per-supplier'),
		//	{
		//		type: 'bar',
		//		data: {
		//			labels: dataSupplier.map(row => row?.nama),
		//			datasets: [
		//				{
		//					label: 'Penjualan per Supplier',
		//					data: dataSupplier.map(row => row?.sum)
		//				}
		//			]
		//		}
		//	}
		//);
	}

	const createChartPerBarang = () => {
		const dataSupplier = <?= json_encode($grafik['totalPenjualanPerBarang']) ?>;
		new Chart(
			document.getElementById('sale-per-barang'),
			{
				type: 'bar',
				data: {
					labels: dataSupplier.map(row => row?.nama),
					datasets: [
						{
							label: 'Penjualan per Barang',
							data: dataSupplier.map(row => row?.sum)
						}
					]
				}
			}
		);
	}

	$(document).ready(function() {
		createChartPerBulan();
		createChartPerSupplier();
		createChartPerBarang();
	})
</script>
