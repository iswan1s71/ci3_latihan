<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title">
					Form Update Supplier
				</h5>
			</div>
			<div class="card-body">
				<form method="POST" action="<?= site_url('supplier/update') ?>">
					<div class="mb-3">
						<label class="form-label">Kode</label>
						<input type="text" class="form-control" value="<?= $data->kode ?>" name="kode">
					</div>
					<div class="mb-3">
						<label class="form-label">Nama</label>
						<input type="text" class="form-control" value="<?= $data->nama ?>" name="nama">
					</div>
					<div class="mb-3">
						<label class="form-label">Status</label>
						<?php $checked = $data->status == 't' ? 'checked' : ''; ?>
						<input type="checkbox" class="form-check-input mt-1" name="status" <?= $checked ?> data-id="<?= $data->id ?>">
					</div>
					<input type="hidden" value="<?= $data->id ?>" name="id">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="<?= site_url('supplier/index') ?>" class="btn btn-dark">
						Kembali
					</a>
				</form>
			</div>
		</div>
	</div>
</div>
