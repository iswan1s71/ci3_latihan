<?php

//namespace App\models;

defined('BASEPATH') OR exit('No direct script access allowed');


class BaseModel extends CI_Model{

    protected $tableName;

	public $last_inserted_id;
	public $last_query_status;
	public $last_error;

	protected function sql($select=null, $join=null, $where=null, $group=null, $order=null, $limit=null)
	{
		$_select = 'SELECT * ';
		if ($select) {
			$_select = "SELECT $select";
		}
		$_join = $join ?? '';
		$_where = $where ?? " WHERE $where";
		$_group = $group ?? " GROUP BY $group";
		$_order = $order ?? " ORDER BY $order";
		$_limit = $limit ?? " LIMIT $limit";
		$sql = $_select . " FROM $this->tableName " . $_join . $_where . $_group . $_order . $_limit;
		return $this->db->query($sql);
	}

	public function findOne($id)
	{
		$query = $this->db->select()
			->where('id', $id)
			->get($this->tableName);
		return $query->row();
	}

	public function findAll()
	{
		$query = $this->db->select()
			->order_by('id', 'ASC')
			->get($this->tableName);

		return $query->result();
	}

    public function create($params)
    {
		$this->db->insert($this->tableName, $params);

		$this->last_inserted_id = $this->db->insert_id();
		$this->last_query_status = $this->db->affected_rows();
    }

    public function update($id, $params)
    {
        $this->db->where('id', $id);
        $this->db->update($this->tableName, $params);

		$this->last_query_status = $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->tableName);

		$this->last_query_status = $this->db->affected_rows();
    }


}

