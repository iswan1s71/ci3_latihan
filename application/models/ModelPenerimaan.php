<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelPenerimaan extends CI_Model
{

	public $tableName = 'penerimaan';

	public function getAll($params = [])
	{
		$select = "$this->tableName.id, 
			$this->tableName.tanggal,
			$this->tableName.tanggal_sj, 
			$this->tableName.no_sj, 
			$this->tableName.no_dokumen AS no_dokumen_penerimaan, 
			p2.no_dokumen AS no_dokumen_pembelian";
		$join = "INNER JOIN pembelian p2 ON p2.id=$this->tableName.id_pembelian::integer ";
		$order = "ORDER BY tanggal_sj desc";
		$where = '';
		if (@$params['count'] != null) {
			$select = "count($this->tableName.id)";
			$order = '';
		}
		if (@$params['where'] != null) {
			$where = ' WHERE ' . $params['where'];
		}
		if (@$params['join'] != null) {
			$join = $params['join'];
		}
		if (@$params['order'] != null) {
			$order = $params['order'];
		}

		$sql = "SELECT $select FROM $this->tableName $join $where $order";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getAllWithDetail($id = null)
	{
		$join = "INNER JOIN pembelian p ON p.id=$this->tableName.id_pembelian ";

		$where = '';
		if ($id != null) {
			$where = " WHERE $this->tableName.id=$id ";
		}

		$select = "$this->tableName.id, 
			$this->tableName.tanggal, 
			tanggal_sj, 
			no_sj,
			$this->tableName.no_dokumen AS no_dokumen_penerimaan, 
			id_pembelian,
			p.no_dokumen AS no_dokumen_pembelian";

		$query = $this->db->query("SELECT $select FROM $this->tableName $join $where ORDER BY id ASC");
		return $query->result_array();
	}

	public function create($params)
	{
		$data = [
			'tanggal' => @$params['tanggal'],
			'tanggal_sj' => @$params['tanggal_sj'],
			'no_sj' => @$params['no_sj'],
			'id_pembelian' => @$params['id_pembelian'],
			'no_dokumen' => @$params['no_dokumen']
		];
		$this->db->insert($this->tableName, $data);

		$last_insert_id = $this->db->insert_id();

		return ($this->db->affected_rows() != 1) ? false : $last_insert_id;
	}

	public function read($id)
	{
//		$query = $this->db->get_where($this->tableName, array('id' => $id));
//		return $query->result_array()[0];
		$query = $this->getAllWithDetail($id);
		return $query->result_array()[0];
	}

	public function update($params)
	{
		$data = [];

		if (@$params['tanggal_sj']) {
			$data['tanggal_sj'] = $params['tanggal_sj'];
		}
		if (@$params['no_sj']) {
			$data['no_sj'] = $params['no_sj'];
		}

		$this->db->where('id', @$params['id']);
		$this->db->update($this->tableName, $data);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tableName);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public static function get($params = [])
	{
		$model = new self();
		$query = $model->getAll();

		if ($params['count'] == true) {
			return count($query);
		}

		return $query;
	}

	public function findAll($params=[])
	{
		$query = $this->db->select();

		if (@$params['like'] != null) {
			$query->like(@$params['like'], @$params['value']);
		}

		$query->get($this->tableName);

		if (@$params['count'] == true) {
			return $query->count_all_results();
		}
		return $query->result();
	}

}

?>
