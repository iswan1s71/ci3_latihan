<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ModelDashboard extends CI_Model{

	public function getTotalPenjualanPerBulan()
	{
		$sql = "SELECT periode, sum(subtotal) FROM (
					SELECT to_char(p.tanggal, 'yyyy-mm') AS periode, pd.qty, pd.harga, (pd.qty*pd.harga) AS subtotal FROM pembelian_detail AS pd 
					INNER JOIN pembelian AS p ON p.id=pd.id_pembelian 
					LEFT JOIN barang AS b ON b.id=pd.id_barang 
					LEFT JOIN supplier AS s ON s.id::integer=p.id_supplier::integer
				) AS sq
				GROUP BY 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getTotalPenjualanPerSupplier()
	{
		$sql = "SELECT nama, sum(subtotal) FROM (
					SELECT s.nama, pd.qty, pd.harga, (pd.qty*pd.harga) AS subtotal FROM pembelian_detail AS pd 
					INNER JOIN pembelian AS p ON p.id=pd.id_pembelian 
					LEFT JOIN barang AS b ON b.id=pd.id_barang 
					LEFT JOIN supplier AS s ON s.id::integer=p.id_supplier::integer
				) AS sq
				GROUP BY 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getTotalPenjualanPerBarang()
	{
		$sql = "SELECT nama, sum(subtotal) FROM (
					SELECT b.nama, pd.qty, pd.harga, (pd.qty*pd.harga) AS subtotal FROM pembelian_detail AS pd 
					INNER JOIN pembelian AS p ON p.id=pd.id_pembelian 
					LEFT JOIN barang AS b ON b.id=pd.id_barang 
					LEFT JOIN supplier AS s ON s.id::integer=p.id_supplier::integer
				) AS sq
				GROUP BY 1
				ORDER BY 2 DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


}
