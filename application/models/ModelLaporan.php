<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ModelLaporan extends CI_Model{

	/**
	 * tables used
	 */
    private $barang = 'barang';
	private $supplir = 'supplir';
	private $pembelian = 'pembelian';
	private $pembelianDetail = 'pembelian_detail';

    public function getAll($params=[])
    {
		$select = 'SELECT s.nama AS nama_supplier, p.tanggal, p.no_dokumen, b.kode AS kode_barang, b.nama AS nama_barang, pd.qty, pd.harga AS harga_satuan, (pd.qty*pd.harga) AS subtotal ';
		$join = 'INNER JOIN pembelian AS p ON p.id=pd.id_pembelian ';
		$join .= 'LEFT JOIN barang AS b ON b.id=pd.id_barang ';
		$join .= 'LEFT JOIN supplier AS s ON s.id::integer=p.id_supplier::integer ';
		$where = '';
		$group = '';
		$limit = '';
		$order = 'ORDER BY 2 DESC';

		$tanggalMulai = null;
		$tanggalAkhir = null;
		if (@$params['tanggal_mulai'] != null) {
			$tanggalMulai = $params['tanggal_mulai'];
		}

		if (@$params['tanggal_akhir'] != null) {
			$tanggalAkhir = $params['tanggal_akhir'];
		}

		if ($tanggalMulai != null && $tanggalAkhir != null) {
			$where = " WHERE p.tanggal >= '$tanggalMulai' AND p.tanggal <= '$tanggalAkhir' ";
		}

		if (@$params['id_supplier'] != null) {
			if ($where != '') {
				$where .= " AND p.id_supplier::integer=".$params['id_supplier'];
			} else {
				$where = " WHERE p.id_supplier::integer=".$params['id_supplier'];
			}
		}

		$sql = "$select FROM pembelian_detail AS pd $join $where $group $limit $order";
//		var_dump($sql); die();
        $query = $this->db->query($sql);
        return $query->result_array();
    }

	public function getAllPenerimaan($params=[])
	{
		/*
		$sql = "SELECT tanggal_sj, no_sj, p2.no_dokumen, b.kode AS kode_barang, b.nama AS nama_barang, pd2.qty AS quantity_order, pd.qty AS quantity_accept, pd.sisa
				FROM penerimaan_detail pd
				INNER JOIN penerimaan p ON p.id=pd.id_penerimaan
				INNER JOIN pembelian p2 ON p2.id=p.id_pembelian
				INNER JOIN pembelian_detail pd2 ON pd2.id=pd.id_pembelian_detail";
		*/
		$select = 'tanggal_sj, no_sj, p2.no_dokumen, b.id AS id_barang, b.kode AS kode_barang, b.nama AS nama_barang, pd2.qty AS quantity_order, pd.qty AS quantity_accept, pd.sisa ';
		$join = 'INNER JOIN penerimaan p ON p.id=pd.id_penerimaan ';
		$join .= 'INNER JOIN pembelian p2 ON p2.id=p.id_pembelian ';
		$join .= 'INNER JOIN pembelian_detail pd2 ON pd2.id=pd.id_pembelian_detail ';
		$join .= 'INNER JOIN barang b ON b.id=pd2.id_barang ';
		$where = '';
		$group = '';
		$limit = '';
		$order = 'ORDER BY 1 DESC';

		$tanggalMulai = null;
		$tanggalAkhir = null;
		if (@$params['tanggal_mulai'] != null) {
			$tanggalMulai = $params['tanggal_mulai'];
		}

		if (@$params['tanggal_akhir'] != null) {
			$tanggalAkhir = $params['tanggal_akhir'];
		}

		if ($tanggalMulai != null && $tanggalAkhir != null) {
			$where = " WHERE tanggal_sj >= '$tanggalMulai' AND tanggal_sj <= '$tanggalAkhir' ";
		}

		if (@$params['id_barang'] != null) {
			if ($where != '') {
				$where .= " AND b.id=".$params['id_barang'];
			} else {
				$where = " WHERE b.id=".$params['id_barang'];
			}
		}

		$sql = "SELECT $select FROM penerimaan_detail AS pd $join $where $group $limit $order";
//		var_dump($sql); die();
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}
