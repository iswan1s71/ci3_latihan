<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'BaseModel.php';

class ModelSupplier extends BaseModel
{

	public function __construct()
	{
		parent::__construct();
		$this->tableName = 'supplier';
	}

}
