<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ModelPembelian extends CI_Model{

    public $tableName = 'pembelian';

	public function getAllDistinct()
	{
		$sql = "SELECT DISTINCT ON (1) id, tanggal, no_dokumen_pembelian, keterangan, tanggal_penerimaan, no_dokumen_penerimaan, nama_supplier, status, status_terima
					FROM (
						SELECT pembelian.id, 
						pembelian.tanggal AS tanggal, 
						pembelian.no_dokumen AS no_dokumen_pembelian, 
						keterangan, p2.tanggal AS tanggal_penerimaan, 
						p2.no_dokumen AS no_dokumen_penerimaan, 
						s.nama as nama_supplier, 
						pembelian.status, 
							CASE 
								WHEN p2.id IS NULL 
									THEN false 
									ELSE true 
								END AS status_terima 
						FROM pembelian 
						LEFT JOIN supplier s ON s.id::integer=pembelian.id_supplier::integer 
						LEFT JOIN penerimaan p2 ON p2.id_pembelian=pembelian.id
						GROUP BY 1, 2, 3, 4, 5, 6 ,7, 8, 9
						ORDER BY pembelian.keterangan ASC	
					) AS sq";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    public function getAll($params=[])
    {
		$statusTerima = "CASE WHEN p2.id IS NULL 
								THEN false 
								ELSE true 
						END AS status_terima ";
        $select = "$this->tableName.id, 
        	$this->tableName.tanggal AS tanggal,
			$this->tableName.no_dokumen AS no_dokumen_pembelian,
			keterangan,
			p2.tanggal AS tanggal_penerimaan,
			p2.no_dokumen AS no_dokumen_penerimaan,
			s.nama as nama_supplier,
			$this->tableName.status, 
			$statusTerima";
        $join = "LEFT JOIN supplier s ON s.id::integer=$this->tableName.id_supplier::integer ";
		$join .= " LEFT JOIN penerimaan p2 ON p2.id_pembelian=$this->tableName.id";
//		$order = "ORDER BY id ASC";
		$order = "ORDER BY $this->tableName.tanggal desc";
		$where = '';
		if (@$params['count'] != null) {
			$select = "count($this->tableName.id)";
			$order = '';
		}
		if (@$params['where'] != null) {
			$where = ' WHERE '.$params['where'];
		}
		if (@$params['join'] != null) {
			$join = $params['join'];
		}
		if (@$params['order'] != null) {
			$order = $params['order'];
		}

		$sql = "SELECT $select FROM $this->tableName $join $where $order";
//		var_dump($sql);
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAllWithDetail($id=null)
    {
        $join = "INNER JOIN pembelian_detail pd ON pd.id_pembelian=$this->tableName.id ";
        $join .= "INNER JOIN supplier s ON s.id=$this->tableName.id_supplier::integer ";
        $join .= "INNER JOIN barang b ON b.id=pd.id_barang ";
		$join .= " LEFT JOIN penerimaan p2 ON p2.id_pembelian=$this->tableName.id";

        $where = '';
        if ($id != null) {
            $where = " WHERE $this->tableName.id=$id ";
        }

		$statusTerima = "CASE WHEN p2.id IS NULL 
								THEN false 
								ELSE true 
						END AS status_terima ";

        $select = "$this->tableName.id, 
        	$this->tableName.no_dokumen, 
        	$this->tableName.status, 
        	$this->tableName.tanggal, 
        	id_barang, 
        	b.nama as nama_barang, 
        	b.harga, 
        	id_supplier, 
        	s.nama as nama_supplier, 
        	pd.qty, 
        	keterangan, 
        	$statusTerima";

        $query = $this->db->query("SELECT $select FROM $this->tableName $join $where ORDER BY id ASC");
        return $query->result_array();
    }

	public function getArrayDokumen()
	{
		$query = $this->db->select()
			->get($this->tableName);

		$arrayDokumen = [];
		foreach ($query->result_array() as $item) {
			$arrayDokumen[] = [
				'id' => $item['id'],
				'no_dokumen' => $item['no_dokumen']
			];
		}

		return $arrayDokumen;
	}

    public function create($params)
    {        
        $data = [
            'id_supplier' => @$params['id_supplier'],
            'tanggal' => @$params['tanggal'],
            'keterangan' => @$params['keterangan'],
			'no_dokumen' => @$params['no_dokumen']
        ];
        $this->db->insert($this->tableName, $data);  
        
        $last_insert_id = $this->db->insert_id();

        return ($this->db->affected_rows() != 1) ? false : $last_insert_id;
    }

    public function read($id)
    {
        $query = $this->db->get_where($this->tableName, array('id' => $id));
        return $query->result_array()[0];
    }

    public function update($params)
    {
        $data = [];

		if (@$params['tanggal']) {
			$data['tanggal'] = $params['tanggal'];
		}
		if (@$params['id_supplier']) {
			$data['id_supplier'] = $params['id_supplier'];
		}
		if (@$params['keterangan']) {
			$data['keterangan'] = $params['keterangan'];
		}
		if (@$params['no_dokumen']) {
			$data['no_dokumen'] = $params['no_dokumen'];
		}
		if (@$params['status']) {
			$data['status'] = $params['status'];
		}
    
        $this->db->where('id', @$params['id']);
        $this->db->update($this->tableName, $data);

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->tableName);

        return ($this->db->affected_rows() != 1) ? false : true;
    }

}
