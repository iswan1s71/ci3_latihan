<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelPenerimaanDetail extends CI_Model
{

	public $tableName = 'penerimaan_detail';

	public function get($select = '', $join = '', $where = '', $limit = '', $order = ' ORDER BY id ASC')
	{
		if ($select == '') {
			$select = '*';
		}

		$sql = "SELECT $select FROM $this->tableName $join $where $limit $order";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getAll()
	{
		$query = $this->db->query("SELECT * FROM $this->tableName");

		return $query->result_array();
	}

	public function create($params)
	{
		$data = [
			'id_penerimaan' => @$params['id_penerimaan'],
			'id_pembelian_detail' => @$params['id_pembelian_detail'],
			'qty' => intval(@$params['qty']),
			'sisa' => intval(@$params['sisa'])
		];
		var_dump($data);
		$this->db->insert($this->tableName, $data);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function read($id)
	{
		$query = $this->db->get_where($this->tableName, array('id' => $id));
		return $query->result_array()[0];
	}

	public function update($params)
	{
		$data = array();

		if (@$params['id_penerimaan']) {
			$data['id_penerimaan'] = $params['id_penerimaan'];
		}
		if (@$params['id_pembelian_detail']) {
			$data['id_pembelian_detail'] = $params['id_pembelian_detail'];
		}
		if (@$params['qty']) {
			$data['qty'] = $params['qty'];
		}
		if (@$params['sisa']) {
			$data['sisa'] = $params['sisa'];
		}
//		var_dump($params);

		$this->db->where('id', @$params['id']);
		$this->db->update($this->tableName, $data);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tableName);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getByIdPenerimaan($id_penerimaan)
	{
		$sql = "SELECT pd.id, 
       				pd.id_pembelian_detail,
					b.id AS id_barang, 
					b.nama AS nama_barang, 
					pd2.harga AS harga_satuan, 
					pd2.qty AS quantity_order,  
       				pd.qty AS quantity_accept, 
       				pd.sisa AS penerimaan_sisa,
       				pd2.sisa AS pembelian_sisa					
				FROM penerimaan_detail pd 
				INNER JOIN pembelian_detail pd2 ON pd2.id=pd.id_pembelian_detail 
				INNER JOIN barang b ON b.id=pd2.id_barang WHERE pd.id_penerimaan=$id_penerimaan";
//		var_dump($sql); die();
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function deleteBy($key, $value)
	{
		$this->db->where($key, $value);
		$this->db->delete($this->tableName);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

}

?>
