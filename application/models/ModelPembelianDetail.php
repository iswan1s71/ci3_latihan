<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ModelPembelianDetail extends CI_Model{

    public $tableName = 'pembelian_detail';

    public function get($select='', $join='', $where='', $limit='', $order=' ORDER BY id ASC'){
        if ($select == '') {
            $select = '*';
        }

        $sql = "SELECT $select FROM $this->tableName $join $where $limit $order";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAll($params=[]){
        $query = $this->db->query("SELECT * FROM $this->tableName");
        return $query->result_array();
    }

    public function create($params)
    {
        $data = [
            'id_pembelian' => @$params['id_pembelian'],
            'id_barang' => @$params['id_barang'],
            'harga' => @$params['harga'],
            'qty' => @$params['qty']
        ];
        $this->db->insert($this->tableName, $data); 

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function read($id)
    {
        $query = $this->db->get_where($this->tableName, array('id' => $id));
        return $query->result_row();
    }

    public function update($id, $params)
    {
//        $data = array();
//
//        if (@$params['id_pembelian']) {
//            $data['id_pembelian'] = $params['id_pembelian'];
//        }
//        if (@$params['id_barang']) {
//            $data['id_barang'] = $params['id_barang'];
//        }
//        if (@$params['harga']) {
//            $data['harga'] = $params['harga'];
//        }
//        if (@$params['qty']) {
//            $data['qty'] = $params['qty'];
//        }
//		if (@$params['sisa']) {
//			$data['sisa'] = $params['sisa'];
//		}
    
        $this->db->where('id', $id);
        $this->db->update($this->tableName, $params);

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->tableName);

        return ($this->db->affected_rows() != 1) ? false : true;
    }

	public function deleteBy($key, $value)
	{
		$this->db->where($key, $value);
		$this->db->delete($this->tableName);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getByIdPembelian($id_pembelian)
	{
		$sql = "SELECT $this->tableName.id, 
					$this->tableName.id_barang, 
					$this->tableName.harga, 
					$this->tableName.qty, 
					$this->tableName.sisa AS sisa_pembelian,
					pd.sisa AS sisa_penerimaan
				FROM $this->tableName
				LEFT JOIN penerimaan_detail AS pd ON pd.id_pembelian_detail=$this->tableName.id
				WHERE $this->tableName.id_pembelian=$id_pembelian
				ORDER BY $this->tableName.id ASC";
//		var_dump($sql); die();
		$query = $this->db->query($sql);
		return $query->result_array();
	}


	public function getByIdPembelianDistinct($id_pembelian)
	{
		$sql = "SELECT DISTINCT ON (1) id, id_barang, harga, qty, sisa_pembelian, sisa_penerimaan
					FROM 
					(
						SELECT pembelian_detail.id, 
						pembelian_detail.id_barang, 
						pembelian_detail.harga, 
						pembelian_detail.qty, 
						pembelian_detail.sisa AS sisa_pembelian, 
						penerimaan_detail.sisa AS sisa_penerimaan 
						FROM pembelian_detail 
						LEFT JOIN penerimaan_detail ON penerimaan_detail.id_pembelian_detail=pembelian_detail.id
						WHERE pembelian_detail.id_pembelian = $id_pembelian
						GROUP BY 1, 2, 3, 4, 5, 6
						ORDER BY id DESC
					) AS sq
					ORDER BY id ASC";
//				var_dump($sql); die();
		$query = $this->db->query($sql);
		return $query->result_array();
	}



}

?>
