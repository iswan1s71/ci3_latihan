<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'BaseModel.php';

class ModelBarang extends BaseModel
{

	public function __construct()
	{
		parent::__construct();
		$this->tableName = 'barang';
	}

}
