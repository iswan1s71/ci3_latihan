<?php

defined('BASEPATH') or exit('No direct script access allowed');

require 'BaseController.php';

class Penerimaan extends BaseController
{

	protected $menu = 'penerimaan';

	public function __construct()
	{
		parent::__construct();
		$this->setActiveMenu($this->menu);

		//load model
		$this->load->model('ModelPenerimaan');
		$this->load->model('ModelPenerimaanDetail');
		$this->load->model('ModelPembelian');
		$this->load->model('ModelPembelianDetail');
		$this->load->model('ModelBarang');
	}

	public function index()
	{
		$allPenerimaan = $this->ModelPenerimaan->getAll();

		$this->render('penerimaan/index', [
			'allPenerimaan' => $allPenerimaan
		]);
	}

	public function create()
	{
		$arrayDokumen = $this->ModelPembelian->getArrayDokumen();

		$post = $this->input->post();
		if ($post != null) {
			$params = [
				'tanggal' => @$post['tanggal'],
				'tanggal_sj' => @$post['tanggal_sj'],
				'no_sj' => @$post['no_sj'],
				'id_pembelian' => @$post['id_pembelian']
			];
			return $this->createShowDetail($params);
		}

		$this->render('penerimaan/create', [
			'arrayDokumen' => $arrayDokumen
		]);
	}

	public function createShowDetail($params=[])
	{
		$allBarang = $this->ModelBarang->getAll();
		$arrayDokumen = $this->ModelPembelian->getArrayDokumen();

		$post = $this->input->post();
		if (@$post['penerimaan_detail'] != null) {
			$this->doCreate($post);
		}

		$allPembelianDetail = $this->ModelPembelianDetail->getByIdPembelian(@$params['id_pembelian']);
		if ($this->checkDuplicatePembelianDetail($allPembelianDetail)) {
			$allPembelianDetail = $this->ModelPembelianDetail->getByIdPembelianDistinct(@$params['id_pembelian']);
		}

		$this->render('penerimaan/create-show-detail', [
			'arrayDokumen' => $arrayDokumen,
			'allPembelianDetail' => $allPembelianDetail,
			'post' => $params,
			'allBarang' => $allBarang
		]);
	}

	private function checkDuplicatePembelianDetail($allPembelianDetail)
	{
		$arrayBarang = [];
		foreach ($allPembelianDetail as $pembelianDetail) {
			if (in_array($pembelianDetail['id_barang'], $arrayBarang)) {
				return true;
			}
			$arrayBarang[] = $pembelianDetail['id_barang'];
		}
		return false;
	}

	public function read($id)
	{
		$allBarang = $this->ModelBarang->getAll();
		$arrayDokumen = $this->ModelPembelian->getArrayDokumen();

		$data = $this->ModelPenerimaan->getAllWithDetail($id)[0];

		$allPenerimaanDetail = $this->ModelPenerimaanDetail->getByIdPenerimaan($id);

		$this->render('penerimaan/read', [
			'data' => $data,
			'allPenerimaanDetail' => $allPenerimaanDetail,
			'arrayDokumen' => $arrayDokumen,
			'allBarang' => $allBarang
		]);
	}

	public function currencyToInt($str = '')
	{
		// hapus prefix Rp.
		$str = str_replace("Rp.", "", $str);

		// hapus 2 digit dibelakang titik
		$str = str_replace(".00", "", $str);

		// hapus comma
		$str = str_replace(",", "", $str);
		return $str;
	}

	public function update($id=null)
	{
		// action  post submit
		$post = $this->input->post();
		if ($post != null) {
			$id = $post['id'];
			$this->doUpdate($post);
		}

		$allBarang = $this->ModelBarang->getAll();
		$arrayDokumen = $this->ModelPembelian->getArrayDokumen();

		$data = $this->ModelPenerimaan->getAllWithDetail($id)[0];

		$allPenerimaanDetail = $this->ModelPenerimaanDetail->getByIdPenerimaan($id);

		$this->render('penerimaan/update', [
			'data' => $data,
			'allPenerimaanDetail' => $allPenerimaanDetail,
			'arrayDokumen' => $arrayDokumen,
			'allBarang' => $allBarang
		]);
	}

	private function doCreate($post)
	{
		$data = [
			'tanggal' => @$post['tanggal'],
			'tanggal_sj' => @$post['tanggal_sj'],
			'no_sj' => @$post['no_sj'],
			'id_pembelian' => @$post['id_pembelian'],
			'no_dokumen' => $this->generateNumberDocument(@$post['tanggal'])
		];

		//create penerimaan and return id
		$last_id_inserted = $this->ModelPenerimaan->create($data);

		if ($last_id_inserted >= 0) {
			//create penerimaan detail
			$success = true;
			foreach (@$post['penerimaan_detail'] as $penerimaanDetail) {
				$detail = [
					'id_penerimaan' => $last_id_inserted,
					'id_pembelian_detail' => $penerimaanDetail['id_pembelian_detail'],
					'qty' => $penerimaanDetail['qty'],
					'sisa' => $penerimaanDetail['sisa']
				];
				// echo '<pre>'; var_dump($detail); echo '</pre>'; die();
				if (!$this->ModelPenerimaanDetail->create($detail)) {
					$success = false;
				}

				// update sisa ke tabel pembelian_detail
				$pembelian = [
					'sisa' => intval($penerimaanDetail['sisa'])
				];
//					var_dump($pembelian);
//					echo $penerimaanDetail['id_pembelian_detail'];
//				echo '<pre>'; var_dump($penerimaanDetail); echo '</pre>';
				if (!$this->ModelPembelianDetail->update($penerimaanDetail['id_pembelian_detail'], $pembelian)) {
					$success = false;
				}
			}

			if ($success) {
				$this->session->set_flashdata('success', "Berhasil disimpan");
				redirect('penerimaan/index');
			} else {
				$this->session->set_flashdata('danger', 'Error detail penerimaan');
			}
		}
	}

	private function doUpdate($post)
	{
		//update penerimaan
		$params = [
			'tanggal_sj' => @$post['tanggal_sj'],
			'no_sj' => @$post['no_sj'],
			'id' => @$post['id']
		];
		if ($this->ModelPenerimaan->update($params)) {
			//update penerimaan detail
			$success = true;

			foreach (@$post['penerimaan_detail'] as $penerimaanDetail) {
				$detail = [
					'id' => $penerimaanDetail['id'],
					'qty' => $penerimaanDetail['qty'],
					'sisa' => $penerimaanDetail['sisa']
				];

				if (!$this->ModelPenerimaanDetail->update($detail)) {
					$success = false;
				}

				// update sisa ke tabel pembelian_detail
				$pembelian = [
					'sisa' => $penerimaanDetail['sisa'],
				];
//					var_dump($pembelian); die();
				if (!$this->ModelPembelianDetail->update($penerimaanDetail['id_pembelian_detail'], $pembelian)) {
					$success = false;
				}
			}

			if ($success) {
				$this->session->set_flashdata('success', 'Update barang berhasil.');
				redirect('penerimaan/index');
			} else {
				$this->session->set_flashdata('danger', 'Terjadi kesalahan.');
			}
		}
	}

	public function delete($id)
	{
		if ($id != null) {
			if ($this->ModelPenerimaan->delete($id)) {
				// delete penerimaan detail detail
				$this->load->model('ModelPenerimaanDetail');
				$this->ModelPenerimaanDetail->deleteBy('id_penerimaan', $id);
				$this->session->set_flashdata('success', 'Hapus data berhasil.');
			}
		}
		redirect ('penerimaan/index');
	}

	public function updateStatus()
	{
		$post = $this->input->post();
		if ($post != null) {
			$params = [
				'id' => @$post['id'],
				'status' => @$post['status']
			];
			if ($this->ModelBarang->update($params)) {
				$this->session->set_flashdata('success', 'Update status berhasil.');
			}
		}
		redirect('barang/index');
	}

	public function getHargaBarang()
	{
		$data = [];

		$post = $this->input->post();
		if (!$post) {
			return data;
		}
		$id_barang = $post['id_barang'];

		$allBarang = $this->ModelBarang->getAll();
		foreach ($allBarang as $barang) {
			$data['id'] = $barang['id'];
			$data['value'] = $barang['harga'];

			if ($id_barang == $data['id']) {
				echo $barang['harga'];
			}
		}

		return $data;
	}

	public function generateNumberDocument($tanggal='')
	{
		/* contoh format nomor dokumen
		"OP-2301-0001";
		*/
		$prefix = 'BTB';
		$delimiter = '-';
		$nowMonth = date('y-m');
		if ($tanggal) {
			$date = strtotime($tanggal);
			$nowMonth = date('y-m', $date);
		}
		$count = $this->getCountPenerimaan($nowMonth) + 1;
		$nextCount = sprintf('%04d', $count);
		$nowMonth = str_replace("-", "", $nowMonth);
		$noDocument = $prefix . $delimiter . $nowMonth . $delimiter . $nextCount;
		return $noDocument;
	}

	private function getCountPenerimaan($periode=null)
	{
		$nowMonth = date('Y-m');
		if ($periode != null) {
			$nowMonth = $periode;
		}
		$like = [
			'like' => "TO_CHAR(penerimaan.tanggal, 'yyyy-mm')",
			'value' => "'%$nowMonth'"
		];
//		"TO_CHAR(penerimaan.tanggal, 'yyyy-mm') like '%$nowMonth'";
		$params = [
			'like' => $like,
			'count' => true,
		];
		$query = $this->ModelPenerimaan->getAll($params);
		return @$query[0]['count'] ?? 0;
	}

}
