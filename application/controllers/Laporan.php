<?php defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require 'BaseController.php';
class Laporan extends BaseController {

	protected $menu = 'laporan';

	public function __construct()
	{
		parent::__construct();
		$this->setActiveMenu($this->menu);

		//load model
		$this->load->model('ModelBarang');
		$this->load->model('ModelSupplier');
		$this->load->model('ModelPembelian');
		$this->load->model('ModelPembelianDetail');
		$this->load->model('ModelLaporan');
	}

	public function index()
	{
		$category=$this->uri->segment(3);

		if ($category == 'penerimaan') {
			return $this->indexPenerimaan();
		}
		return $this->indexPembelian();
	}

	private function indexPenerimaan()
	{
		$arrayBarang = ModelBarang::getArrayNamaBarang();

		$this->menu = 'report-penerimaan';

		$this->render('laporan/index-penerimaan', [
			'arrayBarang' => $arrayBarang,
		]);
	}

	private function indexPembelian()
	{
		$allBarang = $this->ModelBarang->getAll();
		$allSupplier = $this->ModelSupplier->getAll();

		$this->menu = 'report-pembelian';

		$this->render('laporan/index', [
			'allBarang' => $allBarang,
			'allSupplier' => $allSupplier
		]);
	}

	public function generate()
	{
		$post = $this->input->post();
		if (!@$post) {
			$this->session->set_flashdata('danger', 'Request gagal.');
			redirect ('laporan/index');
		}

		$id_supplier = intval(@$post['id_supplier']);
		$params = [
			'tanggal_mulai' => @$post['tanggal_mulai'],
			'tanggal_akhir' => @$post['tanggal_akhir'],
			'id_supplier' => $id_supplier,
		];
		$allData = $this->ModelLaporan->getAll($params);

		if (@$post['export_excel']) {
//			echo '<pre>'; var_dump($allData); echo '</pre>';
			$supplier = '';
			if ($id_supplier > 0){
				$getSupplier = $this->getSupplier(['id_supplier' => $params['id_supplier']]);
				$supplier = $getSupplier['nama'] . '_';
			}
			$filename = 'Laporan_pembelian_' . $supplier .$params['tanggal_mulai'].'-'.$params['tanggal_akhir'] . '.xlsx';
			return $this->exportExcel($allData, $filename);
		}

		$this->render('laporan/read', [
			'allData' => $allData,
			'post' => $post
		]);
	}

	private function exportExcel($allData=[], $filename=null)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A1', "NO");
		$sheet->setCellValue('B1', "NAMA SUPPLIER");
		$sheet->setCellValue('C1', "TANGGAL");
		$sheet->setCellValue('D1', "NO. DOKUMEN");
		$sheet->setCellValue('E1', "KODE");
		$sheet->setCellValue('F1', "NAMA");
		$sheet->setCellValue('G1', "QUANTITY");
		$sheet->setCellValue('H1', "HARGA SATUAN");
		$sheet->setCellValue('I1', "SUBTOTAL");

		$no=1;
		$row=2;
		foreach ($allData as $data) {
			$sheet->setCellValue('A'.$row, $no++);
			$sheet->setCellValue('B'.$row, $data['nama_supplier']);
			$sheet->setCellValue('C'.$row, $data['tanggal']);
			$sheet->setCellValue('D'.$row, $data['no_dokumen']);
			$sheet->setCellValue('E'.$row, $data['kode_barang']);
			$sheet->setCellValue('F'.$row, $data['nama_barang']);
			$sheet->setCellValue('G'.$row, $data['qty']);
			$sheet->setCellValue('H'.$row, $data['harga_satuan']);
			$sheet->setCellValue('I'.$row, $data['subtotal']);
			$row++;
		}

		$row2 = $row+1;
		$sheet->setCellValue('A'.$row2, 'TOTAL');
		$formula = "=SUM(I2:I$row)";
		$sheet->setCellValue('I'.$row2, $formula);

		$FORMAT_CURRENCY_RP_CUSTOM = '[$Rp-421]#,##0.00;[RED]-[$Rp-421]#,##0.00';

		$spreadsheet->getActiveSheet()
			->getStyle("H:H")
			->getNumberFormat()
			->setFormatCode($FORMAT_CURRENCY_RP_CUSTOM);

		$spreadsheet->getActiveSheet()
			->getStyle("I:I")
			->getNumberFormat()
			->setFormatCode($FORMAT_CURRENCY_RP_CUSTOM);

		$sheet->getDefaultRowDimension()->setRowHeight(-1);
		$sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
		$sheet->setTitle("Laporan Data Siswa");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		if ($filename == null) {
			$filename = "Laporan_pembelian_.xlsx";
		}

		header('Content-Disposition: attachment; filename="'. $filename . '"');
		header('Cache-Control: max-age=0');
		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}

	public function generatePenerimaan()
	{
		$post = $this->input->post();
		if (!@$post) {
			$this->session->set_flashdata('danger', 'Request gagal.');
			redirect ('laporan/index-penerimaan');
		}

		$id_barang = intval(@$post['id_barang']);
		$params = [
			'tanggal_mulai' => @$post['tanggal_mulai'],
			'tanggal_akhir' => @$post['tanggal_akhir'],
			'id_barang' => $id_barang,
		];
		$allData = $this->ModelLaporan->getAllPenerimaan($params);

		if (@$post['export_excel']) {
//			echo '<pre>'; var_dump($allData); echo '</pre>';
			$barang = '';
			if ($id_barang > 0){
				$getBarang = $this->getBarang(['id_barang' => $params['id_barang']]);
				$barang = $getBarang['nama'] . '_';
			}
			$filename = 'Laporan_penerimaan_' . $barang .$params['tanggal_mulai'].'-'.$params['tanggal_akhir'] . '.xlsx';
			return $this->exportExcelPenerimaan($allData, $filename);
		}

		$this->render('laporan/read-penerimaan', [
			'allData' => $allData,
			'post' => $post
		]);
	}

	private function exportExcelPenerimaan($allData=[], $filename=null)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A1', "NO");
		$sheet->setCellValue('B1', "TANGGAL SJ");
		$sheet->setCellValue('C1', "NOMOR SJ");
		$sheet->setCellValue('D1', "NO. DOKUMEN");
		$sheet->setCellValue('E1', "KODE BARANG");
		$sheet->setCellValue('F1', "NAMA BARANG");
		$sheet->setCellValue('G1', "QUANTITY ORDER");
		$sheet->setCellValue('H1', "QUANTITY TERIMA");
		$sheet->setCellValue('I1', "SISA");

		$no=1;
		$row=2;
		foreach ($allData as $data) {
			$sheet->setCellValue('A'.$row, $no++);
			$sheet->setCellValue('B'.$row, $data['tanggal_sj']);
			$sheet->setCellValue('C'.$row, $data['no_sj']);
			$sheet->setCellValue('D'.$row, $data['no_dokumen']);
			$sheet->setCellValue('E'.$row, $data['kode_barang']);
			$sheet->setCellValue('F'.$row, $data['nama_barang']);
			$sheet->setCellValue('G'.$row, $data['quantity_order']);
			$sheet->setCellValue('H'.$row, $data['quantity_accept']);
			$sheet->setCellValue('I'.$row, $data['sisa']);
			$row++;
		}

		$sheet->getDefaultRowDimension()->setRowHeight(-1);
		$sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
		$sheet->setTitle("Laporan Data Siswa");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		if ($filename == null) {
			$filename = "Laporan_pembelian_.xlsx";
		}

		header('Content-Disposition: attachment; filename="'. $filename . '"');
		header('Cache-Control: max-age=0');
		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}

	private function getSupplier($params=[])
	{
		$allSupplier = $this->ModelSupplier->getAll();

		if (@$params['id_supplier'] != null) {
			foreach ($allSupplier as $supplier) {
				if ($supplier['id'] == $params['id_supplier']) {
					return $supplier;
				}
			}
		}

		return $allSupplier;
	}

	private function getBarang($params=[])
	{
		$allBarang = $this->ModelBarang->getAll();

		if (@$params['id_barang'] != null) {
			foreach ($allBarang as $barang) {
				if ($barang['id'] == $params['id_barang']) {
					return $barang;
				}
			}
		}

		return $allBarang;
	}

}
