<?php

defined('BASEPATH') OR exit('No direct script access allowed');


//require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require 'BaseController.php';
class Dashboard extends BaseController {

	protected $menu = 'dashboard';

	public function __construct()
	{
		parent::__construct();
		$this->setActiveMenu($this->menu);

		//load model
		$this->load->model('ModelBarang');
		$this->load->model('ModelSupplier');
		$this->load->model('ModelPembelian');
		$this->load->model('ModelPembelianDetail');
		$this->load->model('ModelLaporan');
		$this->load->model('ModelDashboard');
	}

	public function index()
	{
		$allBarang = $this->ModelBarang->findAll();
		$allSupplier = $this->ModelSupplier->getAll();
		$allPembelian = $this->ModelPembelian->getAll();

		$totalPenjualanPerBulan = $this->getPenjualanPerBulan();
		$totalPenjualanPerSupplier = $this->getPenjualanPerSupplier();
		$totalPenjualanPerBarang = $this->getPenjualanPerBarang();

		$grafik = [
			'totalPenjualanPerBulan' => $totalPenjualanPerBulan,
			'totalPenjualanPerSupplier' => $totalPenjualanPerSupplier,
			'totalPenjualanPerBarang' => $totalPenjualanPerBarang
		];

		$this->render('dashboard/index', [
			'allBarang' => $allBarang,
			'allSupplier' => $allSupplier,
			'allPembelian' => $allPembelian,
			'grafik' => $grafik
		]);
	}

	private function getPenjualanPerBulan()
	{
		$data = $this->ModelDashboard->getTotalPenjualanPerBulan();
		return $data;
	}

	private function getPenjualanPerSupplier()
	{
		$data = $this->ModelDashboard->getTotalPenjualanPerSupplier();
		return $data;
	}

	private function getPenjualanPerBarang()
	{
		$data = $this->ModelDashboard->getTotalPenjualanPerBarang();
		return $data;
	}

	public function test()
	{
		$this->load->model('ModelBarang');
		$barang = $this->Barang->findOne(1);
//		var_dump($barang->nama);

//		$newBarang = [
//			'kode' => 'BRU001',
//			'nama' => 'Phone holder',
//			'harga' => 8500
//		];
//		$this->ModelBarang->create($newBarang);

//		if ($this->ModelBarang->last_query_status) {
//			echo 'new barang id : ' . $this->ModelBarang->last_inserted_id;
//		}

		$update = [
			'nama' => 'Phone holder update',
		];
		$this->Barang->update(20, $update);

		echo '=-----------------';

		$allBarang = $this->Barang->findAll();
		foreach ($allBarang as $barang) {
			echo $barang->kode . ' ' . $barang->nama . ' ' . $barang->harga . '<br/>';
		}

//		$this->ModelBarang->delete(1);
//		if ($this->ModelBarang->last_query_status) {
//			echo 'delete success';
//		} else {
//			echo 'delete failed';
//		}
	}
}
