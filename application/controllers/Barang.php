<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use App\helpers\Helper;
require APPPATH . '\helpers\Helper.php';

require 'BaseController.php';

class Barang extends BaseController {

	protected $menu = 'barang';

	protected $model;

	public function __construct()
	{
		parent::__construct();
		$this->setActiveMenu($this->menu);

		//load model
		$this->load->model('ModelBarang');
	}

	public function index()
	{
		$allBarang = $this->ModelBarang->findAll();

		$this->render('barang/index', [
			'allBarang' => $allBarang
		]);
	}

	public function create()
	{
		$post = $this->input->post();
		if ($post != null) {
			$status = 'false';
			if (@$post['status'] == 'on') {
				$status = true;
			}

			$harga = Helper::currencyToInt(@$post['harga']);
			$params = [
				'kode' => @$post['kode'],
				'nama' => @$post['nama'],
				'harga' => $harga,
				'status' => $status
			];
			$this->ModelBarang->create($params);
			if ($this->ModelBarang->last_query_status) {
				$this->session->set_flashdata('success', 'ModelBarang berhasil disimpan.');
				redirect('barang/index');
			}
		}

		$this->render('barang/create');
	}

	public function read($id) 
	{
		$data = $this->ModelBarang->findOne($id);

		$this->render('barang/read', [
			'data' => $data
		]);
	}

	public function update($id=null)
	{
		$post = $this->input->post();
		if ($post != null) {
			$status = 'false';
			if (@$post['status'] == 'on') {
				$status = true;
			}

			$harga = Helper::currencyToInt($post['harga']);
			$params = [
				'kode' => @$post['kode'],
				'nama' => @$post['nama'],
				'harga' => $harga,
				'status' => $status
			];
			$this->ModelBarang->update(@$post['id'], $params);
			
			if ($this->ModelBarang->last_query_status) {
				$this->session->set_flashdata('success', 'Update barang berhasil.');
				redirect('barang/index');
			}
		}	

		$data = $this->ModelBarang->findOne($id);

		$this->render('barang/update', [
			'data' => $data
		]);
	}

	public function delete($id)
	{
		$this->ModelBarang->delete($id);
		if ($this->ModelBarang->last_query_status) {
			$this->session->set_flashdata('success', 'Hapus barang berhasil.');
		} else {
			$this->session->set_flashdata('danger', 'Gagal hapus barang.');
		}
		redirect ('barang/index');
	}

	public function updateStatus()
	{
		$post = $this->input->post();
		if ($post != null) {
			$params = [
				'status'=> @$post['status']
			];
			$this->ModelBarang->update($post['id'], $params);
			if ($this->ModelBarang->last_query_status) {
				$this->session->set_flashdata('success', 'Update status berhasil.');
			}
		}
		redirect('barang/index');
	}

}
